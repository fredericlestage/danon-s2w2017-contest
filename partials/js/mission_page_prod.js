/* START #47748 */

var $scanMission = $('.scan-mission');
var $scanInput = $("#eanCode");
var $scanButton = $scanInput.next('[type="submit"]');
var $currentPoints = $('.block_counter ul');
function validateEan(ean){
	if(!Globals.SIGNED_IN) {
		redirectPushlog();
		return;
	}

	if(ean === "")
		return;

	$('.scan-form .dn-icon-delete').remove();

	var data = {
		p_p_state : 'exclusive',
		p_p_lifecycle : '1',
		p_p_mode : 'view'
	};
	var portletNamespace = $('#danpoint').data("portlet-namespace");
	var genericScanError = $scanMission.data("generic-scan-error");
	var url = $scanMission.data("scan-missions-url");
	data[portletNamespace+"code"] = ean;

	// console.log(url);

	$('<span class="loading"></span>').appendTo($('.scan-form'));

	$('.loading').css('opacity',1);

	$.ajax({
		type: "POST",
		url: url,
		data: data,

		success: function(message, textStatus, jqXHR) {
			$('.scan-form .loading').remove();

			$('section').prepend(message).find('.box:nth-child(2)').remove();

			console.log(jqXHR.getResponseHeader('X-Scan-Status'));
			console.log(jqXHR.getResponseHeader('X-Scan-Response'));

			if (jqXHR.getResponseHeader('X-Scan-Status') == "Success") {
				var response = $.parseJSON(jqXHR.getResponseHeader('X-Scan-Response'));

				console.log(response.pointsBalance);

				var points = response.pointsBalance | 0;

				console.log(points);

				var missionsDone = response.missions;

				if (points) {
					updatePoint(points);
				}
			}
		}
	}).error(function(message){
		console.log(message);
	});
}


function redirectPushlog(){
	// var pushlogUrl = this._$scanSubmit.data("pushlog-url");
 	var pushlogUrl = $scanButton.data("pushlog-url");

	if(pushlogUrl.indexOf('?') == -1) {
		pushlogUrl += '?';
	} else {
		pushlogUrl += '&';
	}

	var currentUrl = location.origin + location.pathname;

	pushlogUrl += 'redirect=' + encodeURIComponent(currentUrl);

	var code = $scanInput.val();
	code = code.split(" ").join("");

	if(code !== null && code.length > 0) {
		if(currentUrl.indexOf('?') == -1) {
			pushlogUrl += encodeURIComponent('?');
		} else {
			pushlogUrl += encodeURIComponent('&');
		}

		pushlogUrl += encodeURIComponent('code=' + code);
	}

	if(pushlogUrl) {
		window.location.href = pushlogUrl;
	}
}

$(function() {
	$('#danpoint').off('submit', '.scan-form');
	$('#danpoint').on('submit', '.scan-form', function(e) {
		e.preventDefault();
		e.stopPropagation();
		if ($('#eanCode').val() == '') return;

		$('<span class="loading"></span>').appendTo($('.scan-form'));
		$('.scan-form .loading').css('opacity', 1);

		var url = Globals.CS_PROXY_URL + "/v2/pincode/" + $('#eanCode').val() + '/check';
		$.ajax({
			url: url,
			type: 'GET',
			dataType: "json",
			contentType: "application/json",
			jsonp: false,
			suppressErrors: true,
			headers: {
				"accept": "application/json",
				"DanoneCountryCode": Globals.COUNTRY,
				"DanoneLanguageCode": Globals.LANGUAGE,
				"DanonePortalBrandCode": Globals.CONFIG.portalBrandCode
			},
			success: function(data) {
				console.log(data);
				if (data.brand == 'ACTIVIA') {
					var htmlFr = '<div class="container-dispatchinter container-dispatchinter-countries"> <div class="container-dispatchinter-countries-header">Voulez-vous relever le défi Activia ?</div> <p style="text-align:center; padding:2em;">Courez la chance de gagner 1 des 3 évasions bien-être de luxe*</p> <p style="text-align:center;"><button class="ext-btn-product-large-dark dn-icon-right-arrow stay-dan-on" style="margin: .4em 0 .4em .4em;">Je reste sur dan-on.com</button> <button class="ext-btn-product-large-dark dn-icon-right-arrow go-activa" style="margin:.4em;">Je participe au défi Activia</button></p> <p style="padding:1.5em; font-size:.8em;"><i>* Valeur de 1 650 $</i></p></div>';
					var htmlEn = '<div class="container-dispatchinter container-dispatchinter-countries"> <div class="container-dispatchinter-countries-header">Do you want to take the Activia Challenge?</div> <p style="text-align:center; padding:2em;">You could win 1 of 3 deluxe wellness getaways*</p> <p style="text-align:center;"><button class="ext-btn-product-large-dark dn-icon-right-arrow stay-dan-on" style="margin: .4em 0 .4em .4em;">I stay on dan-on.com</button> <button class="ext-btn-product-large-dark dn-icon-right-arrow go-activa" style="margin:.4em;">I take the Activia Challenge</button></p> <p style="padding:1.5em; font-size:.8em;"><i>* A $1,650 value</i></p></div>';
					$.colorbox({
						html: $('body').hasClass('language-fr') ? htmlFr : htmlEn,
						width: '600px',
						maxWidth: '90%',
						onComplete: () => {
							$.colorbox.resize();
                            $('.stay-dan-on, #cboxOverlay').on('click', function() {
                                validateEan($('#eanCode').val());
                                $.colorbox.close();
                            });
                            $('.go-activa').on('click', function() {
                                window.open('https://www.challenge.activia.ca/pin/' + $('#eanCode').val());
                                $('#eanCode').val('');
                                $('.scan-form .loading').remove();
                                $.colorbox.close();
                            });
						}
					});
				} else if (data.brand == 'OIKOS') {
                    var htmlFr = '<div class="container-dispatchinter container-dispatchinter-countries"> <div class="container-dispatchinter-countries-header">VOULEZ-VOUS PARTICIPER AU CONCOURS OIKOS<sup>&reg;</sup> LA COLLATION GAGNANTE&nbsp;?</div> <p style="text-align:center; padding:2em;">À GAGNER : PLUS DE 100&nbsp;000&nbsp;$ EN PRIX INSTANTANÉS ET UN GROS LOT DE 10&nbsp;000&nbsp;$</p> <p style="text-align:center;"><button class="ext-btn-product-large-dark dn-icon-right-arrow stay-dan-on" style="margin: .4em 0 .4em .4em;">JE RESTE SUR DAN-ON.CA</button> <button class="ext-btn-product-large-dark dn-icon-right-arrow go-s2w2017" style="margin:.4em;">JE PARTICIPE À OIKOS<sup>&reg;</sup> LA COLLATION GAGNANTE</button></p> <!--p style="padding:1.5em; font-size:.8em;"><i>Le concours débute le 7 mars 2017</i></p--></div>';
					var htmlEn = '<div class="container-dispatchinter container-dispatchinter-countries"> <div class="container-dispatchinter-countries-header">WANT TO ENTER THE OIKOS<sup>&reg;</sup> SNACK TO WIN CONTEST?</div> <p style="text-align:center; padding:2em;">TO BE WON: OVER $100,000 IN INSTANT PRIZES AND A GRAND PRIZE OF $10,000</p> <p style="text-align:center;"><button class="ext-btn-product-large-dark dn-icon-right-arrow stay-dan-on" style="margin: .4em 0 .4em .4em;">STAY ON DAN-ON.CA</button> <button class="ext-btn-product-large-dark dn-icon-right-arrow go-s2w2017" style="margin:.4em;">ENTER OIKOS<sup>&reg;</sup> SNACK TO WIN</button></p> <!--p style="padding:1.5em; font-size:.8em;"><i>Contest starts march 7<sup>th</sup>, 2017</i></p--></div>';
					$.colorbox({
						html: $('body').hasClass('language-fr') ? htmlFr : htmlEn,
						width: '600px',
						maxWidth: '90%',
						onComplete: () => {
							$.colorbox.resize();
                            $('.stay-dan-on, #cboxOverlay').on('click', function() {
                                validateEan($('#eanCode').val());
                                $.colorbox.close();
                            });
                            $('.go-s2w2017').on('click', function() {
                                if($('body').hasClass('language-fr')) {
                                    window.open('https://www.dan-on.com/ca-fr/collation-gagnante?pincode=' + $('#eanCode').val(), '_self');
                                } else {
                                    window.open('https://www.dan-on.com/ca-en/snack-to-win?pincode=' + $('#eanCode').val(), '_self');
                                }
                                $('#eanCode').val('');
                                $('.scan-form .loading').remove();
                                $.colorbox.close();
                            });
						}
					});
                } else {
					validateEan($('#eanCode').val());
				}
			},
			error: function() {
				console.log('error');
				validateEan($('#eanCode').val());
			}
		});
	});
});

/* END #47748 */