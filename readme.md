# Create beautiful pages for Dan-on Oikos Snack to Win Contest 2017.

**General informations**

-The pages are mobile first and developped with html, sass and handlebars.
-The naming convention uses camelCase names followed by an underscore and the relevant specification (language, breakpoint, etc.).

## Stuff you can do and how to do it

**Create a new page**

* Duplicate the template.hbs file in the 'pages' folder into a folder of the name of your page.
* In the folder 'partial/styles', create a folder of the same name as your page and create in it a desktop and mobile sass file for your new page.
* In the gulpfile, under the task 'hbs' -> var 'options' -> parameter 'batch', add a new line with a value of "./partials/generic/styles/[your page]".
* Return to the HBS file you created on the first step and change all the values that are commented out (don't forget the page id).
* Create! And do not forget that there is an english and french version of each page.


**Update a page**

* Update the HTML in pages/[page].hbs
* Update the CSS (SASS) in partials/styles/[page]_[mobile/desktop].scss

## The builder (gulp)
* The default task compiles everything and updates all the pages.
* The task 'watch' does the same thing as the default task with the addition of watching all HBS and CSS files.