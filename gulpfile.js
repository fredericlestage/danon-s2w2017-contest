/* Require vars */
var gulp = require('gulp'),
    hbs = require('gulp-compile-handlebars'),
    sass = require('gulp-sass'),
    ext = require("gulp-ext-replace");

gulp.task('default', ['hbs']);

gulp.task('hbs', ['css_generic', 'css'], function () {
    var templateData = {
        "minWidth": "768"
    }
    var options = {
        batch: ['./partials/generic',
                './partials/generic/styles/',
                './partials/generic/styles/comingSoon',
                './partials/generic/styles/landing',
                './partials/generic/styles/instantWinConfirm',
                './partials/generic/styles/confirmation',
                './partials/generic/styles/rules',
                './partials/generic/styles/concoursTermine'],
        helpers: {}
    }

    return gulp.src(['./pages/**/*.hbs', '!./pages/template.hbs'])
        .pipe(hbs(templateData, options))
        .pipe(ext('.html'))
        .pipe(gulp.dest('./dist'));
});

gulp.task('css_generic', function () {
    return gulp.src('./partials/generic/styles/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(ext('.hbs'))
        .pipe(gulp.dest('./partials/generic/styles'));
});

gulp.task('css', function () {
    return gulp.src('./partials/styles/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(ext('.hbs'))
        .pipe(gulp.dest('./partials/generic/styles'));
});

gulp.task('watch', ['hbs'], function () {
    gulp.watch(['./partials/generic/styles/**/*.scss', './partials/styles/**/*.scss', './pages/**/*.hbs', '!./pages/template.hbs'], ['hbs']);
});
